# redux-arguments-middleware

```bash
npm install redux-arguments-middleware
```

## Motivation
Redux Arguments middleware allows you to encode actions as multiple parameters to the `dispatch` function instead of a single object. When married with other design patterns this can make dispatch calls more succinct. Redux Arguments gives you another call signature to use for dispatch, however the classical "action object" signature is still valid and recognized.

```js
// Dispatch
store.dispatch('ADD', {amount: 1});
store.dispatch('MULTIPLY', {amount: 3});
```

## Installation

To enable Redux Arguments, use [`applyMiddleware()`](https://redux.js.org/api-reference/applymiddleware).

*We recommend you apply Redux Arguments before other middlewares because they generally expect the classic "action object" signature, which won't be available until after Redux Arguments performs its mapping.*

```js
import {createStore,  applyMiddleware} from  'redux';
import  argumentsMiddleware  from  'redux-arguments-middleware';
import  rootReducer  from  './reducers/index';

const  store  =  createStore(
  rootReducer,
  applyMiddleware(
    argumentsMiddleware(),
    // ... other middleware ...
  ),
);
```

## Customization

**Default Behavior:**
```js
const  store  =  createStore(
  rootReducer,
  applyMiddleware(argumentsMiddleware())
);

store.dispatch('ADD', {amount: 1});
// results in...
store.dispatch({type: 'ADD', amount: 1});
```

**Custom Behavior:**

The first parameter to dispatch must always be your action type, but how the rest of your parameters are handled is up to an [optional] `argumentsReducer` function you provide when applying the middleware.

```js
/**
 * Arguments Reducer
 * @param {array} args - All arguments provided to dispatch call (except the first one)
 * @return {object} - Contains everything except the `type` of your final action object.
 */
function argumentsReducer(args) {
  return {amount: args[0]};
};

const  store  =  createStore(
  rootReducer,
  applyMiddleware(
    argumentsMiddleware(argumentsReducer)
  )
);

store.dispatch('ADD', 1);
// store.dispatch({type: 'ADD', amount: 1});

store.dispatch('MULTIPLY', 3);
// store.dispatch({type: 'MULTIPLY', amount: 3});
```
