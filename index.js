const _isString = require('lodash.isstring');

function argumentsMiddleware(argumentsReducer) {
  return () => next => (...action) => {
    if (_isString(action[0])) {
      const [type, ...args] = action;
      return next({type, ...argumentsReducer(args)});
    }
    return next(action[0]);
  };
}

module.exports = argumentsMiddleware;
